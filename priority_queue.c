#include<stdio.h>
#include<stdlib.h>

struct Node
{
    int data;
    struct Node *next;
    int priority;
};

struct Node *new_node(int d, int p)
{
    struct Node *temp= (struct Node *)malloc(sizeof(struct Node));
    temp->data=d;
    temp->next=NULL;
    temp->priority=p;

    return temp;
}

struct Node *head=NULL;
struct Node *r;
int i=0;

void InsertQueueP(struct Node **head, int d, int p)
{
    struct Node *start=(*head);
    struct Node *temp= new_node(d, p);

    if((*head)->priority  > p)
    {
        temp->next = *head;
        (*head)=temp;
    }

    else
    {
        while(start!=NULL && start->next->priority < p )
        {
            start=start->next;
        }
        temp->next=start->next;
        start->next=temp;
    }
}

void display(struct Node *head)
{
    struct Node *ptr=head;
    printf("List:\n");
    while(ptr!=NULL)
    {
        printf("%d ",ptr->data);
        ptr=ptr->next;
    }
    printf("\n");
}

void displayPriority(struct Node *head)
{
    struct Node *ptr=head;
    printf("List Priorities:\n");
    while(ptr!=NULL)
    {
        printf("%d ",ptr->priority);
        ptr=ptr->next;
    }
    printf("\n");
}

void reverselist()
{
    struct Node *prev=NULL;
    struct Node *present=head;
    struct Node *next=NULL;

    while(present!=NULL)
    {
       next=present->next;
       present->next=prev;
       prev=present;
       present=next;
    }
    head=prev;
}

int main()
{
    struct Node *head= new_node(10,2);
    
    InsertQueueP(&head, 20, 8);
    InsertQueueP(&head, 30, 0);
    InsertQueueP(&head, 40, 5);
    InsertQueueP(&head, 50, 7);
    InsertQueueP(&head, 60, 1);
    InsertQueueP(&head, 70, 6);

    display(head);
    displayPriority(head);
    
    //priorityqueue();
    reverselist();
    display(head);
    displayPriority(head);
    return 0;
}
