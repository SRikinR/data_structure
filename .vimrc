set nu                                                                                         
set ts=4                                                                        
set sts=4                                                                       
set sw=4                                                                        
set et                                                                          
set sm                                                                          
set hls                                                                         
set ru                                                                          
set ai                                                                          
set hls                                                                         
set ru                                                                          
set ai                                                                          
set cul                                                                         
set wmnu                                                                        
set ic                                                                          
set smartindent                                                                 
set showmatch                                                                   
set incsearch                                                                   
set laststatus=2                                                                
set ttimeoutlen=50                                                              
"set colorcolumn=80                                                              
set textwidth=80                                                                
set ignorecase                                                                  
"if has("autocmd")                                                               
"  au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$") | exe "normal! g`\"" | endif
"endif                                                                           
                                                                                
if empty(glob('~/.vim/autoload/plug.vim'))                                      
silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs                        
\https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim            
autocmd VimEnter * PlugInstall                                                  
endif                                                                           
                                                                                
                                                                                
call plug#begin('~/.vim/plugged')                                               
Plug 'tpope/vim-surround'                                                       
Plug 'vim-airline/vim-airline'                                                  
Plug 'vim-airline/vim-airline-themes'                                                  
Plug 'altercation/vim-colors-solarized'                                         
Plug 'ervandew/supertab'                                                        
Plug 'vim-scripts/DoxygenToolkit.vim'                                           
Plug 'raimondi/delimitmate'                                                     
"Plug 'bronson/vim-trailing-whitespace'                                          
Plug 'tpope/vim-git'
Plug 'vim-syntastic/syntastic'
Plug 'itchyny/vim-cursorword'
Plug 'preservim/nerdtree'
call plug#end()                                                                 
set tabstop=4                                                                   
set softtabstop=4                                                               
set expandtab                                                                   
set shiftwidth=4                                                                


map <F2> :NERDTreeToggle<CR>
map <F3> :Dox<CR>
map <F4> :DoxLic<CR>
map <F5> :DoxAuthor<CR>


let g:airline_theme= 'simple'
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#left_sep = ' '
let g:airline#extensions#tabline#left_alt_sep = '|'

let g:DoxygenToolkit_authorName="Rikin Shah"
let g:DoxygenToolkit_versionString = "1.0"



nnoremap <C-Left> :bprevious<CR>
nnoremap <C-Right> :bnext<CR>

