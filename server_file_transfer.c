/*
Develop a client server application, wherein client application sends message to server application from STDIN, 
server echoes’ back client Message. Client should display this server response on STDOUT with server processing time. 
Use special keyword “quit” to close both applications gracefully.
*/
#include<stdio.h>
#include<netdb.h>
#include<netinet/in.h>
#include<stdlib.h>
#include<string.h>
#include<sys/socket.h>
#include<sys/types.h>
#include<unistd.h>
#include<sys/time.h>
#include<dirent.h>
#include<sys/stat.h>
#include<time.h>

#define MAX 4096
#define PORT 9898
#define SIZE 50

int sockfd;
char DIR_PATH[100]=".";
char FILE_PATH[100]={0};
char file[SIZE];

int socket_init();
int socket_accept(int);
int socket_close(int);
int take_input(int connfd);
int list_files(int connfd);
int readfile(int connfd);
void printFileProperties(struct stat stats,int connfd);

int main()
{
	int sockfd = socket_init();
	int connfd = socket_accept(sockfd);
	
	int loop = take_input(connfd);
	socket_close(sockfd);
		
	return 0;
}


int socket_init()
{
	int sockfd;
	struct sockaddr_in serveraddr;
	
	// creating socket
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if(sockfd <0)
	{
		printf("Error Creating Socket\n");
		return -1;
	}
	printf("Socket Created\n");
	
	//assign server address
	serveraddr.sin_family=AF_INET;
	serveraddr.sin_port=htons(PORT);
	serveraddr.sin_addr.s_addr=INADDR_ANY;
	
	//	bind the socket with server's address
	if( 0 != bind(sockfd, (struct sockaddr*)&serveraddr, sizeof(serveraddr)) )
	{
		printf("Bind error\n");
		return -1;
		exit(0);
	}
	printf("Bind done\n");

	//listen
	if( 0 != listen(sockfd,5))
	{
		printf("Listening Error\n");
		return -1;
		exit(0);
	}
	printf("Lisening...\n");
	
	return sockfd;
}	

int socket_accept(int sockfd)
{
	//accept
	int connfd, len;
	struct sockaddr_in cli;
	len=sizeof(cli);
	
	connfd = accept(sockfd, (struct sockaddr*)&cli, &len);
	if(connfd < 0)
	{
		printf("client not accepted\n");
		return -1;
	}
	printf("client accepted by the server\n");
	
	return connfd;
}

int socket_close(int sockfd)
{
	if(0 != close(sockfd))
	{
		printf("Error, Socket Not Closed\n");
		return -1;
	}
	printf("Socket Closed\n");
	return 0;
}

int take_input(int connfd)
{
	char input[50];	
	struct stat stats;
	//printf("Server waiting for client input...\n");
	memset(input,'\0',sizeof(input));
	read(connfd, input, sizeof(input));
	if(strcmp(input,"list") ==0 )
    	{
			int connfd_socket = list_files(connfd);
			readfile(connfd_socket);

    		return 1;
    	}
    else if (strcmp(input, "list_metadata") ==0 )
    	{
			int connfd_socket = list_files(connfd);		
			readfile(connfd_socket);
			if (stat(file, &stats) == 0)
    		{
        		printFileProperties(stats,connfd);
    		}
					
			//return 1;
		}
    	
}

int list_files(int connfd)
{
    //printf("***********List Files*****************\n");
    DIR *d;
    struct dirent *dir;
    char str[MAX], str1[MAX];
    memset(str,0,sizeof(str));
    memset(str1,0,sizeof(str1));
    d = opendir(DIR_PATH); // opedir return NULL if error else returns pointer to the director stream 
    if (d)
    {
    	int i=0;
        while ((dir = readdir(d)) != NULL)
        {
            sprintf(str1,"%s\n", dir->d_name);
            strcat(str,str1);    //appends the readed string one by one in str. (at the end of loop, all the file names will be appended into str)
        }
        strcat(str,"\n*************Enter a file name which you want to transfer*************\n");
        closedir(d);
        write(connfd, str, sizeof(str));
        printf("Server sended list to client\n");    
        memset(str,'\0',sizeof(str));
        read(connfd, str, sizeof(str));
        //printf("str = %s\n",str);
		//readfile(connfd);
        
        return connfd;
    }
    else
    {
    	printf("Error in opendir\n");
    	return -1;
    }
}

int readfile(int connfd)
{
	FILE *fp;
	char filename[SIZE], buffer[SIZE];
	char *buffer_ptr;
	long lsize;
	memset(filename, '\0', sizeof(filename));
	read(connfd, filename, sizeof(filename));
	printf("filename = %s\n",filename);
	sprintf(file, "%s", filename);
	printf("file = %s\n",file);


	fp=fopen( file , "r");
	if(fp == NULL)
	{
		printf("file not found or else file is empty\n");
		return -1;
	}
	printf("Opended File: %s\n",file);
	
	fseek(fp,0L,SEEK_END);
	lsize=ftell(fp);
	printf("Size of the file, %s is %ld\n",file, lsize);
	int temp = htonl(lsize);
	write(sockfd, (char*)&temp, sizeof(temp));	
	//printf("Sent Size of the file to client\n");
	rewind(fp);
	
	if(fp==NULL)
	{
		printf("File not found or file is empty\n");
		return -1;
	}
	
	buffer_ptr = calloc(1,lsize+1);
	if (1 != fread(buffer_ptr, lsize, 1, fp) )
	{
		printf("Error, unable to read file\n");
		return -1;
	}
	printf("Fetching File: %s\n",file);
	
	fclose(fp);
	printf("Closed File: %s\n",file);
	printf("\nServer Transferring the file...\n");

	write(connfd, buffer_ptr, lsize);
	printf("Server sent the buffer data to client\n");
	//memset(buffer_ptr,'\0',strlen(buffer_ptr)+1);

}
void printFileProperties(struct stat stats,int connfd)             // chattion to print metadata of file
{
    struct tm dt;
    char prop[MAX],file_size[100],c_time[100],m_time[100];
    // File permissions
    if (stats.st_mode & R_OK)
        strcat(prop,"\nread");
    if (stats.st_mode & W_OK)
        strcat(prop,"\twrite");
    if (stats.st_mode & X_OK)
    {
        strcat(prop,"\texecute");
    }
    sprintf(file_size,"\nFile size: %ld", stats.st_size);
    strcat(prop,file_size);
    dt = *(gmtime(&stats.st_ctime));
    sprintf(c_time,"\nCreated on: %d-%d-%d %d:%d:%d", dt.tm_mday, dt.tm_mon, dt.tm_year + 1900, 
                                              dt.tm_hour, dt.tm_min, dt.tm_sec);
    strcat(prop,c_time);                                          
    dt = *(gmtime(&stats.st_mtime));
    sprintf(m_time,"\nModified on: %d-%d-%d %d:%d:%d", dt.tm_mday, dt.tm_mon, dt.tm_year + 1900, 
                                              dt.tm_hour, dt.tm_min, dt.tm_sec);
    strcat(prop,m_time); 
    write(connfd, prop, sizeof(prop));
	printf("meta data: %s\n",prop);

}

