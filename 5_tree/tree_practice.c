#include<stdio.h>
#include<stdlib.h>

struct Node
{
    int data;
    struct Node *left;
    struct Node *right;
};

int Take_int_input(int data)
{
    while(scanf("%d",&data)==0)
    {
        printf("Invalid Input, Please Enter a Integer.");
        scanf("%*s");
    }
}

struct Node *CreateNode(int data)
{
    struct Node *n=(struct Node*)malloc(sizeof(struct Node));
    n->data=data;
    n->left=NULL;
    n->right=NULL;
    return n;
}

struct Node *Preorder_Transversal(struct Node *root)
{
    if(root!=NULL)
    {
        printf("%d ",root->data);
        Preorder_Transversal(root->left);
        Preorder_Transversal(root->right);
    }
}

struct Node *Inorder_Transversal(struct Node *root)
{
    if(root!=NULL)
    {
        Inorder_Transversal(root->left);
        printf("%d ",root->data);
        Inorder_Transversal(root->right);
    }
}

struct Node *Postorder_Transversal(struct Node *root)
{
    if(root!=NULL)
    {
        Postorder_Transversal(root->left);
        Postorder_Transversal(root->right);
        printf("%d ",root->data);
    }
}

struct Node *Insertion(struct Node *root, int data)
{
    struct Node *ipre=NULL;
    if(root==NULL)
    {
        return NULL;
    }
    while(root!=NULL)
    {
        ipre=root;
        if (data==root->data)
        {
            printf("Already Exist!");
            return 0;
        }
        else if(data<root->data)
        {
            root=root->left;
        }
        else
        {
            root=root->right;
        }
    }
    struct Node *new=CreateNode(data);
    if(data<ipre->data)
    {
        ipre->left=new;
    }
    else
    {
        ipre->right=new;
    }
}

struct Node *Search(struct Node *root,int data)
{
    if(root==NULL)
    {
        return NULL;
    }
    if(data==root->data)
    {
        return root;
    }
    if(data<root->data)
    {
        Search(root->left,data);
    }
    else
    {
        Search(root->right,data);
    }
}

struct Node *Search_Iteration(struct Node *root, int data)
{
    if(root==NULL)
    {
        return NULL;
    }
    while(root!=NULL)
    {
        if(data==root->data)
        {
            return root;
        }
        else if(data>root->data)
        {
            root=root->right;
        }
        else
        {
            root=root->left;
        }
    }
}

struct Node *Inorder_Predecessor(struct Node *root)
{
    root=root->left;
    while(root->right!=NULL)
    {
        root =root->right;
    }
    return root;
}

struct Node *Deletion(struct Node *root, int data)
{
    struct Node *ptr;
    if(root==NULL)
    {
        return NULL;
    }
    if(root->left==NULL && root->right==NULL)
    {
        free(root);
        return NULL;
    }

    if(data<root->data)
    {
        root->left=Deletion(root->left,root->data);
    }
    else if (data>root->data)
    {
        root->right=Deletion(root->right,root->data);
    }
    else
    {
        ptr=Inorder_Predecessor(root);
        root->data=ptr->data;
        root->left=Deletion(root->left,root->data);
    }
    return NULL;
}

int  main()
{
    struct Node *p=CreateNode(10);

    return 0;
}