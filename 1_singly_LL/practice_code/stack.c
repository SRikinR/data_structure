#include<stdio.h>
#include<stdlib.h>

struct stack
{
    int size;
    int top;
    int *arr;
};

void Displaylist(struct stack *ptr)
{
    for(int i=0; i<ptr->size;i++)
    {
        printf("%d ",ptr->arr[i]);
    } 
}

int IsFull(struct stack *ptr)
{
     if(ptr->top == (ptr->size -1))
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

int IsEmpty(struct stack *ptr)
{
    if(ptr->top == -1)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

int Push(struct stack *ptr, int value)
{
    if(IsFull(ptr))
    {
        printf("Stack is Full\n");
    }
    else
    {
        ptr->top++;
        ptr->arr[ptr->top]=value;
    }
}

int Pop(struct stack *ptr)
{
    if(IsEmpty(ptr))
    {
        printf("Stack is Empty\n");
    }
    else
    {
        int value = ptr->arr[ptr->top];
        ptr->top--;
        return value;
    }
}

int main()
{
    struct stack *s1=(struct stack *)malloc(sizeof(struct stack));
    s1->size=5;
    s1->top=-1;
    s1->arr=(int *)malloc(s1->size * sizeof(int));
    printf("Stack Created Successfully!\n");

    Push(s1,30);
    Push(s1,40);
    Push(s1,50);
    Push(s1,60);
    Push(s1,70);
    Displaylist(s1);
    

    printf("Popped %d from the stack\n",Pop(s1));
    printf("Popped %d from the stack\n",Pop(s1));
    Displaylist(s1);


    return 0;
}