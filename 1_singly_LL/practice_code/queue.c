#include<stdio.h>
#include<stdlib.h>

struct queue
{
    int size;
    int f;
    int r;
    int *arr;
};

void Displaylist(struct queue *ptr)
{
    for(int i=0; i<ptr->size;i++)
    {
        printf("%d ",ptr->arr[i]);
    } 
    printf("\n");
}

int IsEmpty(struct queue *ptr)
{
    if(ptr->r == ptr->f)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

int IsFull(struct queue *ptr)
{
    if(ptr->r == ptr->size)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

int Enqueue(struct queue *ptr, int val)
{
    if(IsFull(ptr))
    {
        printf("Queue is Full\n");
    }
    else
    {
        ptr->r++;
        ptr->arr[ptr->r]=val;
        printf("Enqueued Element %d\n",val);
    }
}

int Dequeue(struct queue *ptr)
{
    int a=-1;
    if(IsEmpty(ptr))
    {
        printf("Queue is Empty!");
    }
    else
    {
        ptr->f++;
        a= ptr->arr[ptr->f];
    }
    return a;
}

int InsertNode(struct queue *ptr, int val)
{
    if(IsFull(ptr))
    {
        printf("Queue is Full\n");
    }
    else
    {
        do
        {
            ptr->arr[ptr->r+1]=ptr->arr[ptr->r];
            ptr->r--;
        }while(ptr->r==0);
        ptr->arr[0]=val;
    }
}

int main()
{
    struct queue q1;
    q1.size=5;
    q1.f = q1.r = -1;
    q1.arr=(int *)malloc(q1.size * sizeof(int));
    
    Enqueue(&q1,10);
    Enqueue(&q1,20);
    Enqueue(&q1,30);
    printf("Dequeued Element %d\n",Dequeue(&q1));

    Displaylist(&q1);
    InsertNode(&q1,90);
    Displaylist(&q1);

    return 0;
}