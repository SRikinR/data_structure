#include<stdio.h>
#include<stdlib.h>

struct Node
{
    int data;
    struct Node *next;
};

typedef struct List
{
    struct Node *temp;
}list;

list *create()
{
    list *temp_list=malloc(sizeof(list));
    temp_list->temp=NULL;
    return temp_list;
}

int insertnodeAt(list *temp_list, int value, int index)
{
    struct Node *new_node=(struct Node *)malloc(sizeof(struct Node));
    new_node->data=value;

    if(index == 1)
    {
        temp_list->temp=new_node;
    }

}

int insertnodeat(void *head, int value, int index)
{
    struct Node *ptr = head;
    struct Node *new_node=(struct Node*)malloc(sizeof(new_node));
    new_node->data=value;
    
    if(index==1)
    {
        printf("address of head = %p\n",&head);

    }
    else
    {
        struct Node *q;
        int i=0;
        while(i!=index-1)
        {   
            q=ptr;
            ptr=ptr->next;
            i++;
        }
        q->next=new_node;
        new_node->next = ptr;
    }
}

void display(void *head)
{
    struct Node *ptr=head;

    printf("List:\n");
    while (ptr!=NULL)
    {
        printf("%d ", ptr->data);
        ptr=ptr->next;
    }
    printf("\n");
    

}

int main()
{
    struct Node *head = (struct Node *)malloc(sizeof(struct Node));
    head->data=10;
    head->next=NULL;

    insertnodeat((void*)head, 20, 1);
    
    display((void*)head);
    return 0;
}