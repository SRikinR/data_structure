#include<stdio.h>
#include<stdlib.h>

struct Node
{
    int data;
    struct Node *next;
};

struct Node *f=NULL;
struct Node *r=NULL;

void DisplayList(void)
{
    struct Node *ptr=f;
    printf("Elements are:\n");
    while (ptr!=NULL)
    {
        printf("%d ",ptr->data);
        ptr=ptr->next;
    }
    printf("\n");
}

void Enqueue(int value)
{
    struct Node *n=(struct Node *)malloc(sizeof(struct Node));
    if(n==NULL)
    {
        printf("Queue is Full\n");
    }
    else
    {
        n->data=value;
        n->next=NULL;
        if(f==NULL)
        {
            f=r=n;
        }
        else
        {
            r->next=n;
            r=n;
        }
    }
}

int Dequeue(void)
{
    struct Node *ptr = f;
    int val;
    if(f==NULL)
    {
        printf("Queue is Empty\n");
    }
    else
    {
        f=f->next;
        val= ptr->data;
        free(ptr);
    }
    return val;
}

void InserNode(int value)
{
    struct Node *n=(struct Node *)malloc(sizeof(struct Node));
    if(n==NULL)
    {
        printf("Queue is full\n");
    }
    else
    {
        n->data=value;
        n->next=f;
        f=n;
    }
}

int main()
{
    Enqueue(10);
    Enqueue(20);
    Enqueue(30);
    Enqueue(40);
    DisplayList();

    Dequeue();
    DisplayList();

    InserNode(10);
    DisplayList();

    return 0;
}