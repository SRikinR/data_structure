#include<stdio.h>
#include<stdlib.h>

struct Node
{
	int data;
	struct Node *next;
};

struct Node *top=NULL;

void Push(int value)
{
	struct Node *n=(struct Node*)malloc(sizeof(struct Node));
	if(n==NULL)
	{
		printf("Stack Full\n");
	}
	else
	{
		n->data=value;
		n->next=top;
		top=n;
	}
}

int Pop(void)
{
	struct Node *n=top;
	int val;
	if(top == NULL)
	{
		printf("Stack is Empty\n");
	}
	else
	{
		val = n->data;
		top=n->next;
		free(n);
	}
	return val;
}

int DisplayList(void)
{
	struct Node *ptr =top;
	if(ptr==NULL)
	{
		printf("Stack Overflow\n");
	}
	else
	{
		printf("Stack:\n");
		while (ptr!=NULL)
		{
			printf("%d ",ptr->data);
			ptr=ptr->next;
		}
		printf("\n");
	}
}

int main()
{
	Push(10);
	Push(20);
	Push(30);
	DisplayList();

	printf("Popped %d\n",Pop());	
	DisplayList();
	return 0;
}
