/**
 * @file api.h
 * @brief Singly linked list api library's header file. struct and all the api functions are declared here.
 * @author Shah Rikin R.
 * @version 1.0
 * @date 2022-05-06
 */
#include<stdio.h>
#include<stdlib.h>



/**
 * @brief structure Node, used for maintaining linked list 
 * @param data , it stores the value of the particular node 
 * @param *next , points to the next node.
 */
struct Node
{
    int data;
    struct Node *next;
};
typedef struct Node node;

//day 1
int Initlist(void **,int, int);
int Displaylist(void *);
int InsertNode(struct Node *,int);

//day 2
int InsertNodeAt(void *,int ,int);
int DeleteNode(void *);
int DeleteNodeFrom(void *, int);
int DeleteList(void *);
int ReverseList(void *);
int getListSize(void*, int);

//day3
//
//day 5
int SearchData(void *, int);

struct Node* getTail(struct Node* );
struct Node* partition(struct Node* , struct Node* , struct Node** , struct Node** );
struct Node* quickSortRecur(struct Node* , struct Node* );
void quickSort(struct Node** );
int sortlist(void *, int );
int InsertNodeP(void *, int );


int take_proper_int(); // personal api to make sure that user has enter valid input


