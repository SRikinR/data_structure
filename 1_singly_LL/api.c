/**
 * @file api.c
 * @brief This File contains all the api functions for the singly linked list library code.
 * @author Shah Rikin R.
 * @version 1.0
 * @date 2022-05-06
 */
#include"api.h"

int api_flag=2;  
node *top=NULL;
node *f=NULL;
node *r=NULL;


/**
 * @brief This Functions Initializes the list and creats the list for the given size and also it determins that the wether the  list will act as stack or Queue
 *
 * @param head  Head refrence for initializing list 
 * @param max_size_of_list  maximum size of the list 
 * @param flag  flag that will determine the nature of list (stack or queue)
 *
 * @return  returns '0' on success and returns '-1' on failure and gives error message along with.
 */
int Initlist(void **head,int max_size_of_list, int flag)
{
    if(max_size_of_list <= 0)
    {
        printf("List can't be initialized, insuffecient size\n");
        return -1;
    }
    if(flag==1)
    {
        for(int i=0; i<max_size_of_list; i++)
        {
            printf("enter number:");
            int number;
            scanf("%d",&number);

            node *new_node =malloc(sizeof(node));
            new_node->data=number;
            new_node->next=*head;
            *head=new_node;
        }
        api_flag=flag;  // so that we can use flag value outside the function
    }
    else
    {
        for(int i=0; i<max_size_of_list; i++)
        {
            printf("enter number:");
            int number;
            scanf("%d",&number);

            node *new_node=malloc(sizeof(node));
            new_node->data=number;
            new_node->next=NULL;

            node *ptr=*head;
            if(*head==NULL)
            {
                *head=new_node;
                r=*head;
            }
            else
            {
                while(ptr->next!=NULL)
                {
                    ptr=ptr->next;
                }
                ptr->next=new_node;
                r=new_node;
            }
        }
    }

    if(flag==1)
    {
        printf("Flag Status: %d, List will act as a Stack\n",flag);
    }
    else
    {
        printf("Flag Status: %d, List will act as a Queue\n",flag);
    }
    return 0;
}


/**
 * @brief Displays the list 
 *
 * @param head pointer pointing to head is provided as an argumnet 
 *
 * @return returns '0' on success and '-1' on failure and provide an message along with 
 */
int Displaylist(void *head)
{
    int empty_list_check=0;
    node *ptr=head;
    printf("\nList:\n");
    while(ptr!=NULL)
    {
        empty_list_check++;
        printf("%d ",ptr->data);
        ptr=ptr->next;
    }
    if(empty_list_check==0)
    {
        printf("List Is Empty\n");
        return -1;
    }
    printf("\n");
    return 0;
}


/**
 * @brief Inserts Node in the list, depending on the flag (stack or queue)
 *
 * @param head  pointer pointing to head
 * @param value value we need to add to our list 
 *
 * @return returns '0' on success and '-1' on error along with the message
 */
int InsertNode(struct Node *head,int value)
{
    node *ptr=head;
    if(api_flag==1)
    {
            printf("Stack Insertion, enter number:");
            scanf("%d",&value);
            node *new_node =malloc(sizeof(node));
            new_node->next=ptr->next;
            ptr->next=new_node;
            new_node->data=ptr->data;
            ptr->data=value;
    }
    else
    {
            printf("Queue Insertion, enter number:");
            scanf("%d",&value);

            node *new_node=malloc(sizeof(node));
            new_node->data=value;
            new_node->next=NULL;

            node *ptr=head;
            if(head==NULL)
            {
                printf("List Not Initialized\n");
                return -1;
            }
            else
            {
                while(ptr->next!=NULL)
                {
                    ptr=ptr->next;
                }
                ptr->next=new_node;
                r=new_node;
            }
    }
    return 0;
}


/**
 * @brief Inserts Node at a desired position
 *  
 * @param head  pointer poiniting to head of the list 
 * @param value value we need to add to our list 
 * @param position position at which we need to add our node.
 *
 * @return returns '0' on success and '-1' on error along with the message
 */
int InsertNodeAt(void *head,int value ,int position)
{
   int size = getListSize(head,value);

   printf("Insert Node (enter value):");
   scanf("%d",&value);
   printf("Insert Node at (enter Position):");
   scanf("%d",&position);
   
   node *ptr=head;
   node *q=NULL; 

   node *new_node=(node*)malloc(sizeof(node));

   if(position==1)
   {
       new_node->next=ptr->next;
       ptr->next=new_node;
       new_node->data=ptr->data;
       ptr->data=value;
   }
/*   if(position==1)
   {

   }*/
   
   else if (position<=0 || position>size)
   {
        printf("Invalid Position\n");
        return -1;
   }
   else
   {
       int i=0;
        while(i!=position-1 && ptr->next!=NULL)
        {
            q=ptr;
            ptr=ptr->next;
            i++;
        }
        q->next=new_node;
        new_node->data=value;
        new_node->next=ptr;
        r=new_node;
   }
   return 0;
}

/**
 * @brief Delets Node depending on the nature of list (last node if stack and first node if queue)
 *
 * @param head pointer pointing to head  of the list is send as an funciton argument
 *
 * @return returns '0' on success and '-1' on error along with the message
 */
int DeleteNode(void *head)
{
    if(head==NULL)
    {
        printf("Error: Empty List\n");
        return -1;
    }
    int value;
    node *ptr=head;
    value=ptr->data;
    
    node *q=ptr->next;
    ptr->data=q->data;
    ptr->next=q->next;
    free(q);
    
    printf("Deletion: %d Removed from the List\n", value);
    return 0;
}

/**
 * @brief  deletes node from the particular index on the list
 *
 * @param head pointer pointing to head
 * @param position position which we need no remove form the list 
 *
 * @return returns '0' on success and '-1' on error along with the message
 */
int DeleteNodeFrom(void *head, int position)
{
   int size;
   size = getListSize(head,size);
   printf("Enter the Index Position you want to delete:");
   scanf("%d",&position);
   if(position==1) 
   {
        DeleteNode(head);
   }
   else if(position<=0 || position>size)
   {
        printf("Invalid Position\n");
        return -1;
   }
   else
   {
        node *ptr=head;
        node *q=NULL;
        int i=0;
        while(i!=position-1 && ptr->next!=NULL)
        {
            q=ptr;
            ptr=ptr->next; 
            i++;
        }
        int value;
        value=ptr->data;
        q->next=ptr->next;
        free(ptr);
        printf("Node at index %d, %d is deleted\n",position,value);

   }
   return 0;
}

/**
 * @brief Deletes the whole list 
 *
 * @param head pointer pointing to head of the list 
 *
 * @return returns '0' on success and '-1' on error along with the message
 */
int DeleteList(void *head)
{
    int size;
    size=getListSize(head,size);
    node *ptr=head;
    node *q=NULL;
    if(head==NULL)
    {
        printf("Can't delete list, Error: Empty List\n");
        return -1;
    }
    for(int i=0; i<size; i++)
    {
        if(ptr->next==NULL)
        {
            free(head);
            printf("List Deleted\n");
        }
        else
        {
            DeleteNode(ptr);
        }
    }
    return 0;
    printf("Error, DeleteList\n");
}

/**
 * @brief Displays Reverse list using recurssion
 *
 * @param head pointer pointing to the head of the list 
 *
 * @return 
 */
int ReverseList(void *head)
{
    struct Node *ptr =head;
    struct Node *ptr1 =ptr->next;
    while (ptr1!=r)
    {
        ptr1=ptr1->next;
        ptr=ptr->next;
    }
    printf("%d ",ptr1->data);
    r=ptr;
    if(r==head)
    {
        printf("%d ",ptr->data);        
        printf("\n");
    }
    else
    {
        ReverseList(head);
    }

}

/**
 * @brief Gets the current size of the list 
 *
 * @param head pointer pointing to head
 * @param value this variable will be used to store the size of the list in the function.
 *
 * @return the current size of the list 
 */
int getListSize(void* head, int value)
{
    node *ptr=head;
    value=0;
    while(ptr!=NULL)
    {
        value++;
        ptr=ptr->next;
    }
    return value;
}

/**
 * @brief To find the mid element in the list (for binary search)
 *
 * @param start this variable will be considered as the start point of the list
 * @param last this variable will be considered as the end point of the list
 *
 * @return  it returns the new mid element 
 */
struct Node *mid_element(void *start, void *last)
{
    node *ptr=start;
    if(start==NULL)
    {
        return NULL;
    }
    node *slow=ptr;
    node *fast=ptr->next;
    while(fast!=last)
    {
        fast=fast->next;
        if(fast!=last)
        {
            slow=slow->next;
            fast=fast->next;
        }
    }
    return slow;
}

/**
 * @brief This functions is used to search the element in the list  
 * 
 * @param head pointer pointing to the head of the list 
 * @param searching_method this variable input is used for selecting the seaching method (linear or binary search).
 *
 * @return  return '1' on success and '0' if the element is not in the list along with the message.
 */
int SearchData(void *head, int searching_method)
{
    printf("Enter the Searching Method\nPress 1 --> Linear Search\nPress 2 --> Binary Search(make sure your list is sorted)\n");
    scanf("%d",&searching_method);

    printf("Enter the Value you need to search\n");
    int search_value;
    scanf("%d",&search_value);
    node *ptr=head;
    
    int size;
    size = getListSize(head,size);
    if(searching_method==1)
    {
        for(int i=0; i<size; i++)
        {
            if(ptr->data==search_value)
            {
                printf("Found %d\n",search_value);
                return 1;
            }
            ptr=ptr->next;
        }
        printf("%d not in the list\n",search_value);
        return 0;
    }
    else
    {
        printf("Binary Search...\n");
        node *start =ptr;
        node *last=NULL;
        do
        {
            node *mid=mid_element(start,last);

            if(mid==NULL)
            {
                printf("Middle got NULL, Value not Present or array is not sorted\n");
                return 0;
            }
            if(mid->data==search_value)
            {
                printf("Found %d\n",mid->data);
                return 1;
            }
            else if(mid->data < search_value)
            {
                start=mid->next;
            }
            else
            {
                last=mid;
            }
        }while(last==NULL || last!=start);
        printf("Value Not Present\n");
        return 0;
    }
}

/**
 * @brief  This funciton is used to sort the linked list.
 *
 * @param head pointer pointing to the head of the list 
 * @param sorting_method this variable input determines the sorting method (bubble/quick/radix sort).
 *
 * @return returns '0' on empty list.
 */
int sortlist(void *head, int sorting_method )
{
    printf("Press 1 --> Bubble Sort\nPress 2 --> Quick Sort\nPress 3 --> Radix Sort\n");
    scanf("%d",&sorting_method);
    struct Node *quick_sort_head=head;
    if(sorting_method==1)
    {
        printf("Bubble Sort\n");
        node *ptr=NULL;
        int swapp,i;

        if(head==NULL)
        {
            printf("Empty List\n");
            return 0;
        }
        
        /**
         * below do while loop will check the data of ptr and ptr->next and if ptr->data is greater than ptr->next data then it will swap the data. 
         * the above process will work until the ptr->next is not null mean till the end of file 
         */
        do
        {
            swapp=0;
            ptr=head;
            while(ptr->next!=NULL)
            {
                if(ptr->data > ptr->next->data)
                {
                    int temp = ptr->data;
                    ptr->data=ptr->next->data;
                    ptr->next->data=temp;

                    swapp=1;
                }
                ptr=ptr->next;
            }

        }while(swapp);
    }
    else if(sorting_method==2)
    {
        printf("Quick Sort\n");
        quickSort(&quick_sort_head);
    }
    else
    {
        printf("Radix Sort\n");
    }
}

/**
 * @brief This function will insert the node as per the priority provided
 *
 * @param head pointer pointing to the head of the list.
 * @param priority assigns the priority of the node.
 *
 * @return 
 */
int InsertNodeP(void *head, int priority )
{

}


/**
 * @brief this functions take the valid int input only, if not valid then it ask the user to enter a valid input 
 *
 * @return the valid int input entered by the user
 */
int take_proper_int()
{
    int input;
    while(scanf("%d%*[^\n]%*c",&input)==0)
    {
        printf("-->");
        scanf("%*s");
    }
    return input;
}


