var searchData=
[
  ['initlist_0',['Initlist',['../api_8c.html#aab7209a5511b7ae82def56469860cb2f',1,'Initlist(void **head, int max_size_of_list, int flag):&#160;api.c'],['../api_8h.html#a7426a2fa40007a11c925b53b58fc2f83',1,'Initlist(void **, int, int):&#160;api.c']]],
  ['insertnode_1',['InsertNode',['../api_8c.html#a94b2381e777cacc3f18b3e18888f6ec5',1,'InsertNode(struct Node *head, int value):&#160;api.c'],['../api_8h.html#ad4e5e9d99ed1f2273c6459abea92a8b3',1,'InsertNode(struct Node *, int):&#160;api.c']]],
  ['insertnodeat_2',['InsertNodeAt',['../api_8c.html#afe02b9337bf4618bc69400f1a494e3e8',1,'InsertNodeAt(void *head, int value, int position):&#160;api.c'],['../api_8h.html#a49081847ee1df81a9ebd16c3d21c60ef',1,'InsertNodeAt(void *, int, int):&#160;api.c']]],
  ['insertnodep_3',['InsertNodeP',['../api_8c.html#a3635fb36b608c5f5538dc89bd8b4e26b',1,'InsertNodeP(void *head, int priority):&#160;api.c'],['../api_8h.html#a07cdf1a8010cd2731b9158939d63d792',1,'InsertNodeP(void *, int):&#160;api.c']]]
];
