var searchData=
[
  ['deletelist_0',['DeleteList',['../api_8c.html#afff384a98a7c0461397fcd33fc29944b',1,'DeleteList(void *head):&#160;api.c'],['../api_8h.html#a4c1c7b3b604f6bcbf8e0fc5f8b69e33d',1,'DeleteList(void *):&#160;api.c']]],
  ['deletenode_1',['DeleteNode',['../api_8c.html#a4a4a9779d5b11c1da73c0fd72877e4a5',1,'DeleteNode(void *head):&#160;api.c'],['../api_8h.html#ad4fc0ebf9203f10912d484aadf41d505',1,'DeleteNode(void *):&#160;api.c']]],
  ['deletenodefrom_2',['DeleteNodeFrom',['../api_8c.html#a69214ba2a3603da448b10faa7f62c078',1,'DeleteNodeFrom(void *head, int position):&#160;api.c'],['../api_8h.html#a410c81620e3248782b3621a3f6c279fe',1,'DeleteNodeFrom(void *, int):&#160;api.c']]],
  ['displaylist_3',['Displaylist',['../api_8c.html#a601d06efdaee15bfa31ed48130c1a985',1,'Displaylist(void *head):&#160;api.c'],['../api_8h.html#a30da46d9f63008d79c695a7c95a87f2e',1,'Displaylist(void *):&#160;api.c']]]
];
