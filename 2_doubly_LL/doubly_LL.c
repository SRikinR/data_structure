#include<stdio.h>
#include<stdlib.h>

struct Node
{
    int data;
    struct Node *prev;
    struct Node *next;
};

struct Node *ptr;


struct Node *InsertationAtFirst(struct Node *head, int value)
{
    //struct Node *ptr=(struct Node*)malloc(sizeof(struct Node));
    ptr=(struct Node*)malloc(sizeof(struct Node));    
    struct Node *ptr1=head;
    while(ptr1->prev!=NULL)
    {
        ptr=ptr->next;
    }
    ptr->data=value;
    ptr->next=ptr1;
    ptr->prev=NULL;

    head=ptr;
    return head;
    //free(ptr);
}

struct Node *InsertationAtEnd(struct Node *head, int value)
{
//    struct Node *ptr=(struct Node*)malloc(sizeof(struct Node));
    ptr=(struct Node*)malloc(sizeof(struct Node));    
    struct Node *ptr1=head;
    while (ptr1->next!=NULL)
    {
        ptr1=ptr1->next;
    }
    ptr->data=value;
    ptr->next=NULL;
    ptr->prev=ptr1;
    ptr1->next=ptr;
    
    return head;
}

struct Node *InserAtIntermediate(struct Node *head, int value,int index)
{
    //struct Node *ptr=(struct Node *)malloc(sizeof(struct Node));
    ptr=(struct Node*)malloc(sizeof(struct Node));        
    struct Node *ptr1=head;
    int i=0;
    while(i!=index-1)
    {
        ptr1=ptr1->next;
        i++;
    }
    ptr->data=value;
    ptr->next=ptr1->next;
    ptr->prev=ptr1;
    ptr1->next=ptr;

    return head;

}

void DisplayList(struct Node *head)
{
    struct Node *ptr=head;
    while(ptr!=NULL)
    {
        printf("%d ",ptr->data);
        ptr=ptr->next;
    }
    printf("\n");
}

int main()
{
    struct Node *head=(struct Node*)malloc(sizeof(struct Node));
    struct Node *first=(struct Node*)malloc(sizeof(struct Node));
    struct Node *second=(struct Node*)malloc(sizeof(struct Node));
    struct Node *third=(struct Node*)malloc(sizeof(struct Node));

    head->data=10;
    head->prev=NULL;
    head->next=first;

    first->data=430;
    first->prev=head;
    first->next=second;

    second->data=30;
    second->prev=first;
    second->next=third;

    third->data=410;
    third->prev=second;
    third->next=NULL;

    DisplayList(head);
    head=InsertationAtFirst(head,50);
    DisplayList(head);

    head = InsertationAtEnd(head,60);
    DisplayList(head);

    head=InserAtIntermediate(head,100,2);
    DisplayList(head);

    free(head);
    free(first);
    free(second);
    free(third);
    free(ptr);

    return 0;
}
