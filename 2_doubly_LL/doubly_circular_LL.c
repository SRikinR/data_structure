#include<stdio.h>
#include<stdlib.h>

struct Node
{
    int data;
    struct Node *prev;
    struct Node *next;
};

struct Node *ptr;

struct Node *InsertationAtFirst(struct Node *head, int value)
{
    //struct Node *ptr=(struct Node*)malloc(sizeof(struct Node));
    ptr=(struct Node *)malloc(sizeof(struct Node));
    struct Node *ptr1=head;
    struct Node *ptr2=head->prev;

    ptr1->prev=ptr;
    ptr2->next=ptr;
    ptr->data=value;
    ptr->next=ptr1;
    ptr->prev=ptr2;
    head=ptr;

    return head;
}

struct Node *InsertationAtEnd(struct Node *head, int value)
{
    //struct Node *ptr=(struct Node*)malloc(sizeof(struct Node));
    ptr=(struct Node *)malloc(sizeof(struct Node));
    struct Node *ptr1=head->next;
    struct Node *ptr2=head;
    while (ptr1->next!=ptr2)
    {
        ptr1=ptr1->next;
    }

    ptr->data=value;
    ptr->prev=ptr1;
    ptr->next=ptr2;
    
    ptr1->next=ptr;
    ptr2->prev=ptr;
    
    return head;
}

struct Node *InserAtIntermediate(struct Node *head, int value,int index)
{
    //struct Node *ptr=(struct Node *)malloc(sizeof(struct Node));
    ptr=(struct Node *)malloc(sizeof(struct Node));    
    struct Node *ptr1=head;
    int i=0;
    while(i!=index-1)
    {
        ptr1=ptr1->next;
        i++;
    }
    ptr->data=value;
    ptr->next=ptr1->next;
    ptr->prev=ptr1;
    ptr1->next=ptr;

    return head;
}

struct Node *DeleteAtFirst(struct Node *head)
{
    struct Node *ptr=head;
    struct Node *ptr1=head->next;
    while(ptr->next!=head)
    {
        ptr=ptr->next;
    }
    ptr->next=ptr1;
    ptr1->prev=ptr;
    //free(ptr);
    head = ptr1;
    return head;
}

struct Node *DeleteAtEnd(struct Node *head)
{
    struct Node *ptr=head;
    struct Node *ptr1;
    while(ptr->next!=head)
    {
        ptr=ptr->next;
    }
    ptr1=ptr->prev;
    ptr1->next=ptr->next;
    ptr->next=ptr->prev;
    //free(ptr);
    return head;
}

struct Node *DeleteAtIntermediate(struct Node *head,int index)
{
    struct Node *ptr=head;
    struct Node *ptr1;
    struct Node *ptr2;
    int i=0;
    while (i!=index)
    {
        ptr=ptr->next;
        i++;
    }
    ptr1=ptr->prev;
    ptr2=ptr->next;
    ptr1->next=ptr->next;
    ptr2->prev=ptr1;

    return head;
}

void DisplayList(struct Node *head)
{
    struct Node *ptr=head;
    while(ptr->next!=head)
    {
        printf("%d ",ptr->data);
        ptr=ptr->next;
    }
    printf("%d\n",ptr->data);
}

int main()
{
    struct Node *head=(struct Node*)malloc(sizeof(struct Node));
    struct Node *first=(struct Node*)malloc(sizeof(struct Node));
    struct Node *second=(struct Node*)malloc(sizeof(struct Node));
    struct Node *third=(struct Node*)malloc(sizeof(struct Node));

    head->data=10;
    head->prev=third;
    head->next=first;

    first->data=20;
    first->prev=head;
    first->next=second;

    second->data=30;
    second->prev=first;
    second->next=third;

    third->data=40;
    third->prev=second;
    third->next=head;

    DisplayList(head);
    head=InsertationAtFirst(head,50);
    printf("After Insertation at first\n");
    DisplayList(head);

    InsertationAtEnd(head,60);
    DisplayList(head);

    head=InserAtIntermediate(head,100,2);
    DisplayList(head);

    head=DeleteAtFirst(head);
    DisplayList(head);

    head=DeleteAtEnd(head);
    DisplayList(head);

    DeleteAtIntermediate(head,3);
    DisplayList(head);

    free(head);
    free(first);
    free(second);
    free(third);
    free(ptr);

    return 0;
}
