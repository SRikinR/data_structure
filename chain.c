#include<stdio.h>
#include<stdlib.h>
    
struct Node *head=NULL;

struct Node
{
    int data;
    struct Node *next;
};

struct Node *InsertNode()
{
    struct Node *new_node=(struct Node*)malloc(sizeof(struct Node));
    struct Node *ptr=head;
    printf("Enter the Value of the new node:\n");
    int value;
    scanf("%d",&value);
    new_node->data=value;
    new_node->next=NULL;
    if(head==NULL)
    {
        head=new_node;
    }
    else
    {
        while(ptr->next!=NULL)
        {
            ptr=ptr->next;
        }
        ptr->next=new_node;
        
    }

}

void display(struct Node *head)
{
    struct Node *ptr=head;
    printf("List\n");
    while(ptr!=NULL)
    {
        printf("%d ",ptr->data);
        ptr=ptr->next;
    }
}

int check_loop(struct Node *head)
{
    struct Node *ptr=head;
    struct Node *ptr1=head;

    while(1)
    {
        ptr=ptr->next;
        ptr1=ptr1->next->next;
        if(ptr->data=ptr1->data)
        {
            printf("Its a chain loop\n");
            return 1;
        }
        if(ptr==NULL)
        {
            printf("It's a simple linked list\n");
            return 0;
        }
    }
}

int main()
{
    int i=0;
    while(i<5)
    {
        InsertNode();
        i++;
    }

   /* 
    head=(struct Node *)malloc(sizeof(struct Node));
    struct Node *one=(struct Node *)malloc(sizeof(struct Node));
    struct Node *two=(struct Node *)malloc(sizeof(struct Node));
    struct Node *three=(struct Node *)malloc(sizeof(struct Node));
    struct Node *four=(struct Node *)malloc(sizeof(struct Node));

    head->data=10;
    head->next=two;

    one->data=1;
    one->next=two;

    two->data=2;
    two->next=three;

    three->data=3;
    three->next=four;

    four->data=4;
    four->next=one;
*/
    check_loop(head);
    //display(head);
    return 0;
}
