// Tree Userdefined all functions included
#include<stdio.h>
#include<stdlib.h>

struct Node
{
    int data;
    struct Node *left;
    struct Node *right;
};

int Take_int_input(int data)
{
    while(scanf("%d",&data)==0)
    {
        printf("Invalid Input, Please Enter a number.");
        scanf("%*s");
    }
    return data;
}

struct Node *CreateNode(int data)
{
    struct Node *new=(struct Node *)malloc(sizeof(struct Node));
    new->data=data;
    new->left=NULL;
    new->right=NULL;
    return new;
}

struct Node *Insertion(struct Node *root,int data)
{
    struct Node *ptr=NULL;
    if(root==NULL)
    {
        return NULL;
    }
    while(root!=NULL)
    {
        ptr=root;
        if(data==root->data)
        {
            printf("%d, Already Exist\n",root->data);
            return 0;
        }
        else if(data<root->data)
        {
            root=root->left;
        }
        else
        {
            root=root->right;
        }
    }
    struct Node *new=CreateNode(data);
    if(data<ptr->data)
    {
        ptr->left=new;
    }
    else
    {
        ptr->right=new;
    }
}

struct Node *PreTransversal(struct Node *root)
{
    if(root!=NULL)
    {
        printf("%d ",root->data);
        PreTransversal(root->left);
        PreTransversal(root->right);
    }
}

struct Node *InorderTransversal(struct Node *root)
{
    if(root!=NULL)
    {
        InorderTransversal(root->left);
        printf("%d ",root->data);
        InorderTransversal(root->right);
    }
}

struct Node *PostTransversal(struct Node *root)
{
    if(root!=NULL)
    {
        PostTransversal(root->left);
        PostTransversal(root->right);
        printf("%d ",root->data);
    }
}

// Search Node using recursion
struct Node *SearchNode(struct Node *root,int data)
{
    if(root==NULL)
    {
        return NULL;
    }
    if(data==root->data)
    {
        return root;
    }
    if(data<root->data)
    {
        SearchNode(root->left,data);
    }
    else
    {
        SearchNode(root->right,data);
    }
}

//node search without recursion
struct Node *SearchIter(struct Node *root,int data)
{
    if(root==NULL)
    {
        return NULL;
    }
    while(root!=NULL)
    {
        if(data==root->data)
        {
            return root;
        }
        else if (data<root->data)
        {
            root=root->left;
        }
        else
        {
            root=root->right;
        }
    }
}

struct Node *InorederPredecessor(struct Node *root)
{
    root=root->left;
    while (root->right!=NULL)
    {
        root=root->right;
    }
    return root;
    
}

// Node Deletion
struct Node *DeleteNode(struct Node *root,int data)
{
    struct Node *ptr;
    if(root==NULL)
    {
        return NULL;
    }
    if(root->left == NULL && root->right==NULL)
    {
        free(root);
        return NULL;
    }

    if(data<root->data)
    {
        root->left=DeleteNode(root->left,data);
    }
    else if (data>root->data)
    {
        root->right=DeleteNode(root->right,data);
    }
    else
    {
        ptr=InorederPredecessor(root);
        root->data=ptr->data;
        root->left=DeleteNode(root->left,ptr->data);
    }
    return root;
}

struct Node *User_Tree(struct Node *p)
{
    int process=1;
    while(process!=0)
    {
        printf("\nWhat do you want to do?\n\n");
        printf("1=>PreTransversal\n");
        printf("2=>In-orderTransversal\n");
        printf("3=>PostTransversal\n");
        printf("4=>Insertion\n");
        printf("5=>Deletion\n");
        printf("6=>Search\n");
        printf("7=>Search Iteration\n");
        printf("0==>EXIT\n");

        int choice=0;
        //scanf("%d",&choice);
        choice = Take_int_input(choice);

        switch (choice)
        {
            case 0:
                process=0;
                break;
            case 1:
                PreTransversal(p);
                printf("\n");
                break;
            case 2:
                InorderTransversal(p);
                printf("\n");
                break;
            case 3:
                PostTransversal(p);
                printf("\n");                
                break;
            case 4:
            {
                int data=0;
                printf("Enter data to be Inserted\n");
                data=Take_int_input(data);
                Insertion(p,data);  
                break;
            }
            case 5:
            {
                int data=0;
                printf("Enter data to be Deleted\n");
                data=Take_int_input(data);
                DeleteNode(p,data);
                break;
            }
            case 6:
            {
                int data=0;
                printf("Enter data to be Searched\n");
                data = Take_int_input(data);
                struct Node *search= SearchNode(p,data);
                if(search==NULL)
                {
                    printf("Element not found\n");
                }
                else
                {
                    printf("Found: %d\n",search->data);
                }
                break;
            }
            case 7:
            {
                int data=0;
                data =Take_int_input(data);
                SearchIter(p,data);
                struct Node *search= SearchIter(p,data);
                if(search==NULL)
                {
                    printf("Element not found\n");
                }
                else
                {
                    printf("Found: %d\n",search->data);
                }
                break;
            }
            default:
                printf("In-valid Input\n");
        }
    }
}

int main()
{
    // Node Creation
    struct Node *p=CreateNode(5);
    // BST implementation
    printf("Enter the Maximum Size:\n");
    int Max_Size;
    Max_Size=Take_int_input(Max_Size);

    for(int i=0; i<Max_Size;i++)   
    {
        printf("Enter data\n");
        int data;
        data = Take_int_input(data);
        // node Insertion
        Insertion(p,data);
    }

    User_Tree(p);
    
    return 0;
}   
