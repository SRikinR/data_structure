#include<stdio.h>
#include<stdlib.h>

struct Node 
{
    int data;
    struct Node *left;
    struct Node *right;
};

struct Node *CreateNode(int data)
{
    struct Node *new=(struct Node *)malloc(sizeof(struct Node));
    new->data=data;
    new->left=NULL;
    new->right=NULL;
    return new;
}

struct Node *Insert_Node(struct Node *root, int key)
{
    struct Node *prev=NULL;
    while (root!=NULL)
    {
        prev=root;
        if(key==root->data)
        {
            printf("\nInsertion Denied, %d is Already in the BST\n",key);
            return 0;
        }
        else if (key <root->data)
        {
            root=root->left;
        }
        else
        {
            root=root->right;
        }
    }
    struct Node *new=CreateNode(key);
    if(key<prev->data)
    {
        prev->left=new;
    }
    else
    {
        prev->right=new;
    }
}

int main()
{
    struct Node *p=CreateNode(5);
    struct Node *p1=CreateNode(3);
    struct Node *p2=CreateNode(6);
    struct Node *p3=CreateNode(1);
    struct Node *p4=CreateNode(4);

//              5
//            /   \ 
//          3     6
//        /   \ 
//      1       4

    p->left=p1;
    p->right=p2;
    p1->left=p3;
    p1->right=p4;

    Insert_Node(p,7);
    printf("Inserted Data: %d\n",p->right->right->data);
    
    Insert_Node(p,7);
    
    Insert_Node(p,2);
    printf("Inserted Data: %d\n",p->left->left->right->data);

    return 0;
}