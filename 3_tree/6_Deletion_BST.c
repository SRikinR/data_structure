#include<stdio.h>
#include<stdlib.h>

struct Node
{
    int data;
    struct Node *left;
    struct Node *right;
};

struct Node *CreateNode(int data)
{
    struct Node *new=(struct Node *)malloc(sizeof(struct Node));
    new->data=data;
    new->left=NULL;
    new->right=NULL;
    return new;
}

struct Node *Inorder_Transversal(struct Node *root)
{
    if(root!=NULL)
    {
        Inorder_Transversal(root->left);
        printf("%d ",root->data);
        Inorder_Transversal(root->right);
    }
    
}

struct Node *SearchNode(struct Node *root, int key)
{
    while(root!=NULL)
    {
        if(key==root->data)
        {
            return root;
        }
        else if(key<root->data)
        {
            root=root->left;
        }
        else
        {
            root=root->right;
        }
    }
    return NULL;
}

struct Node *InorederPredecessor(struct Node *root)
{
    root=root->left;
    while (root->right!=NULL)
    {
        root=root->right;
    }
    return root;
}

struct Node *DeleteNode(struct Node *root,int key)
{
    struct Node *ptr;
    if(root==NULL)
    {
        return NULL;
    }
    if(root->left == NULL && root->right==NULL)
    {
        free(root);
        return NULL;
    }
    //search for the node to be deleted
    if(key<root->data)
    {
        root->left=DeleteNode(root->left,key);
    }
    else if (key>root->data)
    {
        root->right=DeleteNode(root->right,key);
    }
    //deletion strategy when nood is found
    else
    {
        ptr = InorederPredecessor(root);
        root->data=ptr->data;
        root->left=DeleteNode(root->left,ptr->data);
    }
    return root;
    

}

int main()
{
    struct Node *p=CreateNode(5);
    struct Node *p1=CreateNode(3);
    struct Node *p2=CreateNode(6);
    struct Node *p3=CreateNode(1);
    struct Node *p4=CreateNode(4);
/*
            5
          /   \ 
        3     6
      /   \ 
    1       4
*/

    p->left=p1;
    p->right=p2;
    p1->left=p3;
    p1->right=p4;

    Inorder_Transversal(p);
    printf("\n");

    DeleteNode(p,5);
    
    Inorder_Transversal(p);
    printf("\n");
    
    printf("Current Root = %d\n",p->data);

    //InorederPredecessor(p);

    return 0;
}
