#include<stdio.h>
#include<stdlib.h>

struct Node
{
    int data;
    struct Node *left;
    struct Node *right;
};

struct Node *CreateNode(int data)
{
    struct Node *new=(struct Node *)malloc(sizeof(struct Node));
    new->data=data;
    new->left=NULL;
    new->right=NULL;
    return new;
}

struct Node *Insertion(struct Node *root,int data)
{
    struct Node *ptr=NULL;
    if(root==NULL)
    {
        return NULL;
    }
    while(root!=NULL)
    {
        ptr=root;
        if(data==root->data)
        {
            printf("%d, Already Exist\n",root->data);
            return 0;
        }
        else if(data<root->data)
        {
            root=root->left;
        }
        else
        {
            root=root->right;
        }
    }
    struct Node *new=CreateNode(data);
    if(data<ptr->data)
    {
        ptr->left=new;
    }
    else
    {
        ptr->right=new;
    }
}

struct Node *PreTransversal(struct Node *root)
{
    if(root!=NULL)
    {
        printf("%d ",root->data);
        PreTransversal(root->left);
        PreTransversal(root->right);
    }
}

struct Node *InorderTransversal(struct Node *root)
{
    if(root!=NULL)
    {
        InorderTransversal(root->left);
        printf("%d ",root->data);
        InorderTransversal(root->right);
    }
}

struct Node *PostTransversal(struct Node *root)
{
    if(root!=NULL)
    {
        PostTransversal(root->left);
        PostTransversal(root->right);
        printf("%d ",root->data);
    }
}

// Search Node using recursion
struct Node *SearchNode(struct Node *root,int data)
{
    if(root==NULL)
    {
        return NULL;
    }
    if(data==root->data)
    {
        return root;
    }
    if(data<root->data)
    {
        SearchNode(root->left,data);
    }
    else
    {
        SearchNode(root->right,data);
    }
}

//node search without recursion
struct Node *SearchIter(struct Node *root,int data)
{
    if(root==NULL)
    {
        return NULL;
    }
    while(root!=NULL)
    {
        if(data==root->data)
        {
            return root;
        }
        else if (data<root->data)
        {
            root=root->left;
        }
        else
        {
            root=root->right;
        }
    }
}

struct Node *InorederPredecessor(struct Node *root)
{
    root=root->left;
    while (root->right!=NULL)
    {
        root=root->right;
    }
    return root;
    
}

// Node Deletion
struct Node *DeleteNode(struct Node *root,int data)
{
    struct Node *ptr;
    if(root==NULL)
    {
        return NULL;
    }
    if(root->left == NULL && root->right==NULL)
    {
        free(root);
        return NULL;
    }

    if(data<root->data)
    {
        root->left=DeleteNode(root->left,data);
    }
    else if (data>root->data)
    {
        root->right=DeleteNode(root->right,data);
    }
    else
    {
        ptr=InorederPredecessor(root);
        root->data=ptr->data;
        root->left=DeleteNode(root->left,ptr->data);
    }
    return root;
}

int main()
{
    // Node Creation
    struct Node *p=CreateNode(5);

    // BST implementation
    printf("Press '-1' to EXIT\n");
    for(int i=0; i=-1;i++)
    {
        printf("Enter data\n");
        int data=0;
        scanf("%d",&data);
        if(data==-1)
        {
            break;
        }
        // Node Insertion 
        else
        {
            Insertion(p,data);
        }
    }

    // Transversals pre,post and in-order 
    PreTransversal(p);
    printf("\n");
    InorderTransversal(p);
    printf("\n");
    PostTransversal(p);
    printf("\n");


    // Node Searching 
    struct Node *search = SearchNode(p,5);
    if(search==NULL)
    {
        printf("Element Not Found\n");
    }
    else
    {
        printf("Found: %d\n",search->data);
    }

    //Node Deletion
    DeleteNode(p,3);
    InorderTransversal(p);
    printf("\n");

    //Iteration Searching
    struct Node *search_iter = SearchIter(p,3);
    if(search_iter==NULL)
    {
        printf("Element Not Found\n");
    }
    else
    {
        printf("Found: %d\n",search_iter->data);
    }

    printf("Current Root: %d\n",p->data);

    return 0;
}