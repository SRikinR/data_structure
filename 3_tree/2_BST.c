#include<stdio.h>
#include<stdlib.h>

struct Node
{
    int data;
    struct Node *left;
    struct Node *right;
};

struct Node *CreateNode(int data)
{
    struct Node *new=(struct Node *)malloc(sizeof(struct Node));
    new->data=data;
    new->left=NULL;
    new->right=NULL;
    return new;
}

// For Every Binary Search Tree Inorder Trnasisation gives the Ascending Order of the tree
// In order means   [Left->root->right]
void Inorder_Transversal(struct Node *root)
{
    if(root!=NULL)
    {
        Inorder_Transversal(root->left);        //first sees the left side of tree
        printf("%d ",root->data);               // prints the data
        Inorder_Transversal(root->right);       // sees the right side of tree
    }
}

//Another way to see wheather the given tree is BST or not 
int Is_BST(struct Node *root)
{
    struct Node *prev =NULL;
    if(root!=NULL)
    {
        if(!Is_BST(root->left))
        {
            return 0;
        }
        if (prev!=NULL && root->data <= prev->data)
        {
            return 0;
        }
        prev =root;
        return Is_BST(root->right);
    }
    else
    {
        return 1;
    }
}

int main()
{
    struct Node *p=CreateNode(5);
    struct Node *p1=CreateNode(3);
    struct Node *p2=CreateNode(6);
    struct Node *p3=CreateNode(1);
    struct Node *p4=CreateNode(4);

//              5
//            /   \ 
//          3     6
//        /   \ 
//      1       4

    p->left=p1;
    p->right=p2;
    p1->left=p3;
    p1->right=p4;

    Inorder_Transversal(p);
    printf("\n");

    //printf("Is it a BST?:%d",Is_BST(p));
    //printf("\n");

    return 0;
}