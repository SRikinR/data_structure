#include<stdio.h>
#include<stdlib.h>

struct Node
{
    int data;
    struct Node *left;
    struct Node *right;
};

struct Node *createNode(int data)
{
    struct Node *n=(struct Node *)malloc(sizeof(struct Node));
    n->data=data;
    n->left=NULL;
    n->right=NULL;
    return n;
}

// Pre-order means   [root->Left->right]
void Preorder_Traversal(struct Node *root)
{
    if(root!=NULL)
    {
        printf("%d ",root->data);
        Preorder_Traversal(root->left);
        Preorder_Traversal(root->right);
    }
}

// In order means   [Left->root->right]
void Inorder_Traversal(struct Node *root)
{
    if(root!=NULL)
    {
        Inorder_Traversal(root->left);
        printf("%d ",root->data);
        Inorder_Traversal(root->right);
    }
}

// Post-order means   [Left->right->root]
void Postorder_Traversal(struct Node *root)
{
    if(root!=NULL)
    {
        Postorder_Traversal(root->left);
        Postorder_Traversal(root->right);
        printf("%d ",root->data);
    }
}

int main()
{
    struct Node *p=createNode(4);
    struct Node *p1=createNode(1);
    struct Node *p2=createNode(6);
    struct Node *p3=createNode(5);
    struct Node *p4=createNode(2);

    p->left=p1;
    p->right=p2;
    p1->left=p3;
    p1->right=p4;

    printf("Preoreder_transversal = ");
    Preorder_Traversal(p);
    printf("\n");

    printf("Inoreder_transversal = ");
    Inorder_Traversal(p);
    printf("\n");

    printf("Postoreder_transversal = ");
    Postorder_Traversal(p);
    printf("\n");

   return 0;
}