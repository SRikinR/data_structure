#include<stdio.h>
#include<stdlib.h>

struct Node
{
    int data;
    struct Node *left;
    struct Node *right;
};

struct Node *CreateNode(int data)
{
    struct Node *new=(struct Node *)malloc(sizeof(struct Node));
    new->data=data;
    new->left=NULL;
    new->right=NULL;
}

struct Node *Search_Iter(struct Node *root, int key)
{
    while(root!=NULL)
    {
        if(key==root->data)
        {
            return root;
        }
        else if(key<root->data)
        {
            root=root->left;
        }
        else
        {
            root=root->right;
        }
    }
    return NULL;
}

int main()
{
    struct Node *p=CreateNode(5);
    struct Node *p1=CreateNode(3);
    struct Node *p2=CreateNode(6);
    struct Node *p3=CreateNode(1);
    struct Node *p4=CreateNode(4);

//              5
//            /   \ 
//          3     6
//        /   \ 
//      1       4

    p->left=p1;
    p->right=p2;
    p1->left=p3;
    p1->right=p4;

    struct Node *n=Search_Iter(p,6);
    if(n!=NULL)
    {
        printf("Found: %d\n",n->data);
    }
    else
    {
        printf("Element not found\n");
    }

    return 0;
}