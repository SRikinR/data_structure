/*
   Develop a client server application, wherein client application sends message to server application from STDIN, 
   server echoes’ back client Message. Client should display this server response on STDOUT with server processing time. 
   Use special keyword “quit” to close both applications gracefully.
   */
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include<unistd.h>
#include <sys/socket.h>
#include<netinet/in.h>
#include <arpa/inet.h>


#define MAX 4096
#define PORT 9898
#define SIZE 50

char file_name[100];

void func(int sockfd)
{
    char buff[MAX], filename[MAX];
    int loop=1;
    int flag=0;
    printf("Enter 'list' to show files\nEnter 'list_metadata' to see the list along with its properties\n");
    printf("-->");
    scanf("%s",buff);
    if(strncmp(buff,"list",4) == 0 )
    {
        if(strcmp(buff,"list_metadata") == 0 )
        {
             flag=1;
        }
       
        write(sockfd, buff, sizeof(buff));
        memset(buff,'\0',sizeof(buff));
        read(sockfd, buff, sizeof(buff));
        printf("\nList received from server is:\n%s\n",buff);

        memset(buff,'\0',sizeof(buff));
        printf(">>");
        scanf("%s",filename);
        printf("\nfilename = %s\n",filename);
        write(sockfd, filename, sizeof(filename)); 

        //memset(buff, '\0',sizeof(buff));
        //read(sockfd, buff, 35);
        //printf("Server Query: %s\n",buff);
        
        printf("Client waiting for server response...\n");
        
        char buffer_recv[MAX];
        memset(buff, '\0', sizeof(buff));

        int n=1;
        //receiving buffer length from the server 
        int buffer_len;
        n=read(sockfd,(char*)&buffer_len,sizeof(buffer_len));
        buffer_len=ntohl(buffer_len);

        // using the above received buffer length as a size arg. and take whole buffer input in once. 
        n = read(sockfd, buff, buffer_len);
        printf("Buffer read = %s\n",buff);
        //while((n = read(sockfd, buff, buffer_len)) !=0)
        //{
        //    strcat(buffer_recv,buff);
        //    memset(buff, '\0', sizeof(buff));
        //}
        strcpy(buffer_recv,buff);
        if(flag ==1)
        {
            memset(buff, '\0', sizeof(buff));
            read(sockfd, buff, sizeof(buff));
            printf("meta data is %s\n",buff);
        }
        printf("client received the buffer data\n");
        FILE *fp;
        fp=fopen("received_file.txt","w");
        printf("File Transferring...\n");
        fprintf(fp, "%s", buffer_recv);
        fclose(fp); 
        printf("File Transfered!\n");           
    }
    
    else
    {
        printf("Invalid Input, Enter Again\n");
    }
    
}

int main()
{
    int sockfd, connfd;
    struct sockaddr_in servaddr;

    // socket create and verification
    sockfd = socket(AF_INET, SOCK_STREAM, 0);           //connection oriented
    if (sockfd == -1) 
    {
        printf("socket creation failed...\n");
        exit(0);
    }
    else
    {
        printf("Socket successfully created..\n");
    }

    memset(&servaddr,'\0', sizeof(servaddr));
    // assign IP, PORT
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = INADDR_ANY; //inet_addr("192.168.3.77");
    servaddr.sin_port = htons(PORT);

    // connect the client socket to server socket
    if (connect(sockfd, (struct sockaddr*)&servaddr, sizeof(servaddr)) != 0) 
    {
        printf("connection with the server failed...\n");
        exit(0);
    }
    else
    {
        printf("connected to the server..\n");
    }
    // function for chat
    func(sockfd);

    // close the socket
    close(sockfd);
}

