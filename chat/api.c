// all the functions of the chat application

#include"api.h"

client_t *cl[max];
pthread_mutex_t mutex1 = PTHREAD_MUTEX_INITIALIZER;
int c_sock;
char c_name[20],print[max];

//server function
void add_client(client_t *cli)
{
    pthread_mutex_lock(&mutex1);
    for(int i=0; i<max; i++)
    {
        if(!cl[i])
        {
            cl[i]=cli;
            break;
        }
    }
    pthread_mutex_unlock(&mutex1);
}

//remove client
void rm_c(int uid)
{
    pthread_mutex_lock(&mutex1);
    for(int i=0; i<max; i++) 
    {
        if(cl[i]->uid == uid)
        {
            cl[i]=NULL;
            break;
        }
    }
    pthread_mutex_unlock(&mutex1);
}

// client handler function
void *client_handler(void *cl)
{
    char buff[buff_size],name[10];
    int flag =0;
    int rv,name_len=0;
    client_t *cli=(client_t*)cl;

    if(recv(cli->sockfd,name,10,0)<=0 || strlen(name)<2 || strlen(name)>10)
    {
        printf("Wrong Input\n");
        flag=1;
    }
    else
    {
        strcpy(cli->name,name);
        name_len=strlen(cli->name);
        sprintf(buff,"\n%s has joined,userid to communicate is %d\n",cli->name,cli->uid);
        printf("sockfd is %d\n",cli->sockfd);
        printf("%s",buff);
    }
    while(1)
	{
		char data[1024];
		int read;
        char *ptr;
        bzero(data,sizeof(data));
        if((read = recv(cli->sockfd,data,1024,0))>0)
		{
            printf("data is %s\n",data);
            if(data[name_len+2]=='s' && data[name_len+3]=='e' && data[name_len+4]=='n'&& data[name_len+5]=='d')
            {           
                int id = data[name_len+7]-48;
                //data[read] = '\0';
                ptr=strchr(data,'-');
                send_msg(ptr,id);	              
            }
            strcpy(print,cli->name);
            strcat(print,ptr);
            pthread_cond_signal(&condition);
        }
	}
	
    // while(1)
    // {
    //     memset(buff,0,buff_size);
    //     if(flag)
    //     {
    //         break;
    //     }
        
    //     if(rv=recv(cli->sockfd, buff, buff_size,0)>0)
    //     {
    //         printf("%s: %s\n",cli->name, buff);
    //         // if(strncmp(data,"SEND",4) == 0)
    //         // {
    //         //     int read;
    //         //     printf("ENTERD");
    //         //     read = recv(c_sock,data,1024,0);
    //         //     data[read] = '\0';
    //         //     int id = atoi(data) - 1;
    //         //     read = recv(c_sock,data,1024,0);
                
    //         //     data[read] = '\0';
    //         //     printf("Cli[id].sockID is %d\n",cli[id].sockfd);
    //         //     send_msg(data,id);	
                
    //         // }
    //     }
    //     else  if(strlen(buff) > 0)
    //     {
    //         send_msg(buff, cli->uid);
    //         //send_msg(buff, cli->sockfd);
    //         printf("%s",buff);
    //     }
    //     else if(rv == 0 || strcmp(buff, "quit") == 0)
    //     {
    //         sprintf(buff, "%s has left\n", cli->name);
    //         printf("%s",buff);
    //         //str_trim(buff);
    //         send_msg(buff, cli->uid);
    //         flag = 1;
    //     }
    //     else
    //     {
    //         perror("error while recving message\n");
    //         flag = 1;
    //     }
    //     strcpy(data,buff);
    //     pthread_cond_signal(&condition);
    // }
    // close(cli->sockfd);
    // rm_c(cli->uid);
    // free(cli);
    // pthread_detach(pthread_self());
}


void* update_database(void *args)
{
    while(1)
    {
        pthread_mutex_lock(&mutex1);
        pthread_cond_wait(&condition,&mutex1);
        FILE *fp;
        fp=fopen("database.txt","a+");
        fputs(print, fp);
        fclose(fp);
        pthread_mutex_unlock(&mutex1);   
    }
}
void send_msg(char *buff,int uid)
{
    pthread_mutex_lock(&mutex1);

    for(int i=0; i<max; i++)
    {
        if(cl[i])
        {
            if(cl[i]->uid == uid)
            {
                printf("cl[i]->sockfd is %d",cl[i]->sockfd);
                if ((write(cl[i]->sockfd, buff, strlen(buff))) < 0)
                {
                    perror("ERROR sening your message\n");
                    break;
                }
              
            }
        }
    }
    pthread_mutex_unlock(&mutex1);
}

// client functions
void *send_msg_handler()
{
    char message[buff_size]={0};
    char buffer[buff_size+32]={0};

    while(1)
    {
        fgets(message,buff_size,stdin);
        str_trim(message);

        if(strcmp(message,"quit")==0)
        {
            exit(0);
            break;
        }
        else if(strlen(message)>1)
        {
            sprintf(buffer,"%s: %s\n",c_name,message);
            write(c_sock,buffer,strlen(buffer));
        }
         bzero(message, buff_size);
         bzero(buffer, buff_size + 32);
    }
    quit(2);
}

void *recv_msg_handler()
{
    char message[buff_size]={};
    int receive;
    while(1)
    {
        if(receive=read(c_sock,message,buff_size) > 0)
        {
            printf("%s",message);
        }
        else if(receive==0)
        {
            break;
        }
        memset(message,0,sizeof(message));
    }
}

void quit(int sig)
{
    printf("\nClosing the Connections\n");
    exit(0);
}

void catchFD(int sockfd, char name[10])
{
    c_sock = sockfd;
    for (int i = 0; i < 10; i++)
        c_name[i] = name[i];
}

void str_trim(char *arr)
{
    for (int i = 0; i < strlen(arr); i++)
        if (arr[i] == '\n')
        {
            arr[i] = '\0';
            break;
        }
}


