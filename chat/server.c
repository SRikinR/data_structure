// server code

#include "api.h"

void signal_handler()
{
    printf("exiting...\n");
    exit(0);
}

int main()
{
	signal(SIGINT, signal_handler);

	struct sockaddr_in server;
	int sock_s, sock_c, uid = 0;
	socklen_t c = sizeof(struct sockaddr_in);
	pthread_t thread,book;

	//handle signal
	//signal(SIGINT, signal_handler);

	//open socket
	if((sock_s = socket(AF_INET, SOCK_STREAM, 0)) == 0)
	{
		perror("socket could not open\n");
		exit(-1);
	}

	printf("socket created successfully\n");

	//assigning ports to socket
	server.sin_family = AF_INET;
	server.sin_port = htons(port);
	server.sin_addr.s_addr = INADDR_ANY;

	//binding
	if(bind(sock_s, (struct sockaddr*)&server, sizeof(server)) < 0)
	{
		perror("error binding\n");
		exit(-1);
	}
    
	printf("binding successfully\n");
	
	//listening
	if(listen(sock_s, 10) < 0)
	{
		perror("listen failed\n");
	}

	printf("listening to incoming client\n");

	while(1)
	{
		//accept connection request
		sock_c = accept(sock_s, (struct sockaddr*)&server, &c);

		//check for max clients
		if(uid == max)
		{
			perror("client full\n");
			exit(0);
		}

		client_t *cl = (client_t *)malloc(sizeof(client_t));

		cl->addr = server;
		cl->sockfd = sock_c;
		cl->uid = ++uid;

		//add client into structures
		add_client(cl);

		//creating a thread to each client
		pthread_create(&thread, NULL, client_handler, (void*)cl);
		pthread_create(&book,NULL,update_database,NULL);

	}

	return 0;

}


