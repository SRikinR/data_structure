// header function
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<pthread.h>
#include<sys/socket.h>
#include<signal.h>
#include<arpa/inet.h>
#include<unistd.h>
#include<time.h>
#include <dirent.h>
#include<signal.h>

#define port 8989
#define max 10
#define buff_size 1024

pthread_cond_t condition;
typedef struct
{
    struct sockaddr_in addr;
    int sockfd;
    int uid;
    char name[10];
}client_t;


void add_client(client_t *);                                                                                                                                        
void rm_c(int);                                                                 
void* client_handler(void*);                                                    
void send_msg(char*, int);                                                      
void* send_msg_handler();                                                       
void* recv_msg_handler();  
void* update_database();                                                     
void quit(int);                                                                 
void catchFD(int sockfd, char name[10]);                                        
void str_trim(char *);                                                          



