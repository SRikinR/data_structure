// client code for chat application

#include"api.h"

void signal_handler(int sig)
{
    printf("Exiting...\n");
    exit(0);
}

void list_files()
{
    DIR *di;
    struct dirent *direc;
    di=opendir(".");
    if(di)
    {
        while ((direc = readdir(di)) != NULL)                       
        {                                                           
            printf("%s\n", direc->d_name);                                                                                              
        }                                                           
        closedir(di);
    }
}
int main()
{
    signal(SIGINT,signal_handler);

    struct sockaddr_in client;
    socklen_t c =sizeof(client);
    int sock_c;
    char name[10];
    char name_f[10];
    int i=1;
    time_t t;

    t=time(NULL);
    struct tm tm =*localtime(&t);

    printf("Please Enter Your Name:\t");
    scanf("%s",name);

    while(1)
    {
        printf("Press 1--> To Join the chat room\nPress 0--> To EXIT\n");

        scanf("%d",&i);
        if(i==1)
        {
            // socket client code
            if((sock_c = socket(AF_INET,SOCK_STREAM,0)) == 0)
            {
                perror("Socket Failed to create\n");
                return -1;
            }

            //save socket id and client name
            catchFD(sock_c,name);
            printf("socket created successfully\n");

            client.sin_family = AF_INET;
            client.sin_port = htons(port);

            if(inet_pton(AF_INET, "127.0.0.1", &client.sin_addr) <= 0)
            {
                printf("invalid ip address\n");
                exit(-1);
            }

            if((connect(sock_c,(struct sockaddr *)&client,c)) < 0)
            {
                perror("Connection failed\n");
                exit(-1);
            }

            send(sock_c, name, 10, 0);
            printf("\n=== WELCOME TO THE CHATROOM ===\n\n");
            printf("             %d-%d-%d\n", tm.tm_mday, tm.tm_mon+1, tm.tm_year+1900);

            pthread_t send_msg_thread;
            if(pthread_create(&send_msg_thread,NULL,(void*)send_msg_handler,NULL)!=0)
            {
                perror("Client Send Thread Not Created\n");
                exit(-1);
            }

            pthread_t recv_msg_thread;
            if(pthread_create(&recv_msg_thread,NULL,(void*)recv_msg_handler,NULL)!=0)
            {
                printf("Client, receive thread not created\n");
                return EXIT_FAILURE;
            }
            while(1);
        }
        else
        {
            return 0;
        }
    }
    return 0;
}


